## 使用说明
1. 本地启动mysql，创建transfer数据库；
2. 使用建表脚本初始化数据库；（bin/dbscript/create_transfer_db.sql）
3. 配置数据库信息（src/main/resources/jdbc.properties)，根据实际情况修改数据库连接，用户名和密码；
4. 根目录下mvn clean install, 打包，并使用tomcat启动应用，或使用IDE导入Maven工程后在IDE中调试;
5. 浏览器中访问：http://localhost:8080/api/v1/transfer ,模拟转账请求；（端口根据实际配置而定）


## 设计思路
根据要求以分布式场景为背景实现转账接口，转账双方的账户不在一个数据库里，这是典型的分布式一致性问题。常见的解决分布式一致性问题的办法有TCC事务补偿，和依赖可靠消息保证最终一致性。为了少引入消息队列组件，决定使用TCC的方式实现。

TCC事务补偿的实现：（所有代码均在CapitalTradeServiceImpl类中）

try阶段，对应record()方法，主要功能是插入转账记录（tradeOrder）然后扣除转出资金（预留资源）;

confirm阶段，对应confirmRecord()方法，主要功能是确认转账记录状态和转入资金；

cancel阶段，对应cancelRecord()方法，主要功能是在异常时取消业务，释放预留的资源。

为了便于演示，虽然将转账的两个账户都建在了同一个数据库和表中，但转账的流程模拟了分布式的场景，在TransferController.transfer()方法中，模拟了转账的过程，实际情况可能是使用rpc框架调用远程服务，此处使用本地service代替。


## 其它关注点
### 接口规范
1. 接口设计中带上接口版本号，便于后期接口升级后的维护；
2. 接口入参将公共参数与业务参数分开，便于业务参数校验和业务参数的修改；
3. 公共参数须包含时间戳和渠道号
### 安全 （未实现，仅在代码中相应位置加了注释说明）
1. 对接口增加时间戳校验
2. 对请求进行渠道校验
3. 对业务参数字段进行签名和验签
### 幂等
在TCC的三个阶段，每个阶段都保证了幂等，校验商户流水号，同样的流水号不会重复操作。
### 并发
在TCC的try阶段，并发的请求有可能同时插入tradeOrder，会抛出DataIntegrityViolationException异常，因为record()方法有事务控制，抛出Runtime异常后回滚，并不需要在record()方法中做处理，只需要在上层抓住异常，友好的返回即可

在TCC的confirm和cancel阶段，并发的请求也会造成更新tradeOrder出现问题，故做了乐观锁的设计，TradeOrderRepository.update()方法中实现。

### 异常处理
1. 扣款时余额不足，会抛出InsufficientBalanceException异常（继承RuntimeException, 保证事务回滚）
2. 并发情况，导致插入流水记录（tradeOrder）时可能抛出DataIntegrityViolationException异常；
3. 对于异常处理，在TCC三个阶段中均无需特别处理，只要保证异常抛出后能事务回滚，在上层抓出后根据异常类型友好的返回错误信息即可。
### 用户投诉（未实现）
1. 拟新增通过商户流水号查询转账记录的接口，用于投诉发生时的查询验证；
2. 拟新增通过交易流水，冻结转账金额的接口，通过交易流水查询到转账记录，确认状态为“CONFIRM”, 冻结toUserId账户中的转账金额（有可能出余额不足的情况，认为此情况合理，正常抛错，即认为该笔转账无法追回）；
3. 拟新增转账撤销接口，通过交易流水查询到转账记录，验证状态为CONFIRM，交换fromUserId和toUserId，发起逆向的转账，但扣款须从冻结的资金中扣除。

