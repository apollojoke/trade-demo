package com.springapp.mvc.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by ErikLee on 2019/5/6.
 */
public class TradeOrderTest {

    private TradeOrder testOrder;

    private String draftStatus = "DRAFT";
    private String confirmStatus = "CONFIRM";
    private String cancelStatus = "CANCEL";

    @Before
    public void setUp() throws Exception {
        testOrder = new TradeOrder(1l, 2l, "testMerchantOrderNo", BigDecimal.TEN);
    }

    @Test
    public void shouldConfirmSuccess() throws Exception {
        assertEquals(testOrder.getStatus(), draftStatus);
        testOrder.confirm();
        assertEquals(testOrder.getStatus(), confirmStatus);
    }

    @Test
    public void shouldCancelSuccess() throws Exception {
        assertEquals(testOrder.getStatus(), draftStatus);
        testOrder.cancel();
        assertEquals(testOrder.getStatus(), cancelStatus);

    }
}