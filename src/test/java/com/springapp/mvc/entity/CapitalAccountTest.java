package com.springapp.mvc.entity;


import com.springapp.mvc.exception.InsufficientBalanceException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * Created by ErikLee on 2019/5/6.
 */
public class CapitalAccountTest {

    CapitalAccount testAccount;

    @Before
    public void setUp() throws Exception {
        this.testAccount = new CapitalAccount();
        testAccount.setBalanceAmount(BigDecimal.valueOf(100));
    }

    @Test
    public void shouldTransferToSuccess() throws Exception {
        testAccount.transferTo(BigDecimal.TEN);
        assertEquals(testAccount.getBalanceAmount(), BigDecimal.valueOf(110));
        assertEquals(testAccount.getTransferAmount(), BigDecimal.TEN);
    }

    @Test
    public void shouldTransferFromSuccess() throws Exception {
        testAccount.transferFrom(BigDecimal.TEN);
        assertEquals(testAccount.getBalanceAmount(), BigDecimal.valueOf(90));
        assertEquals(testAccount.getTransferAmount(), BigDecimal.valueOf(-10));
    }

    @Test(expected = InsufficientBalanceException.class)
    public void shouldTransferFromFailed() throws Exception {
        testAccount.transferFrom(BigDecimal.valueOf(200));
        assertEquals(testAccount.getBalanceAmount(), BigDecimal.valueOf(-100));
    }

    @Test
    public void shouldCancelTransferSuccess() throws Exception {
        testAccount.transferFrom(BigDecimal.TEN);
        assertEquals(testAccount.getBalanceAmount(), BigDecimal.valueOf(90));
        assertEquals(testAccount.getTransferAmount(), BigDecimal.valueOf(-10));
        testAccount.cancelTransfer(BigDecimal.TEN);
        assertEquals(testAccount.getBalanceAmount(), BigDecimal.valueOf(100));
        assertEquals(testAccount.getTransferAmount(), BigDecimal.ZERO);
    }
}