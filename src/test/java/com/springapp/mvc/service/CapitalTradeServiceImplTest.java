package com.springapp.mvc.service;

import com.springapp.mvc.dto.TransferOrderDto;
import com.springapp.mvc.entity.CapitalAccount;
import com.springapp.mvc.entity.TradeOrder;
import com.springapp.mvc.repository.CapitalAccountRepository;
import com.springapp.mvc.repository.TradeOrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by ErikLee on 2019/5/6.
 */
public class CapitalTradeServiceImplTest {

    @Mock
    CapitalAccountRepository capitalAccountRepository;

    @Mock
    TradeOrderRepository tradeOrderRepository;

    @InjectMocks
    private CapitalTradeServiceImpl capitalTradeService;

    TransferOrderDto testOrderDto;
    TradeOrder testTradeOrder;
    CapitalAccount fromAccount;
    CapitalAccount toAccount;

    @Before
    public void setUp() throws Exception {
        testOrderDto = new TransferOrderDto();
        testOrderDto.setFromUserId(1001);
        testOrderDto.setToUserId(1002);
        testOrderDto.setMerchantOrderNo("test001");
        testOrderDto.setAmount(BigDecimal.ONE);
        testTradeOrder = new TradeOrder(1001, 1002, "test001", BigDecimal.ONE);
        fromAccount = new CapitalAccount();
        fromAccount.setUserId(1001);
        fromAccount.setBalanceAmount(BigDecimal.TEN);
        toAccount = new CapitalAccount();
        toAccount.setUserId(1002);
        toAccount.setBalanceAmount(BigDecimal.TEN);
        MockitoAnnotations.initMocks(this);
        when(capitalAccountRepository.findByUserId(testOrderDto.getFromUserId())).thenReturn(fromAccount);
        when(capitalAccountRepository.findByUserId(testOrderDto.getToUserId())).thenReturn(toAccount);
    }

    @Test
    public void shouldRecordSuccess() throws Exception {
        capitalTradeService.record(testOrderDto);
        assertEquals(fromAccount.getBalanceAmount(), BigDecimal.valueOf(9));
        assertEquals(fromAccount.getTransferAmount(), BigDecimal.ONE.negate());
    }

    @Test
    public void shouldConfirmSuccess() throws Exception {
        when(tradeOrderRepository.findByMerchantOrderNo(testOrderDto.getMerchantOrderNo())).thenReturn(testTradeOrder);
        capitalTradeService.confirmRecord(testOrderDto);
        assertEquals(testTradeOrder.getStatus(), "CONFIRM");
        assertEquals(toAccount.getBalanceAmount(), BigDecimal.valueOf(11));
        assertEquals(toAccount.getTransferAmount(), BigDecimal.ONE);
    }

    @Test
    public void shouldCancelSuccess() throws Exception {
        when(tradeOrderRepository.findByMerchantOrderNo(testOrderDto.getMerchantOrderNo())).thenReturn(testTradeOrder);
        capitalTradeService.cancelRecord(testOrderDto);
        assertEquals(testTradeOrder.getStatus(), "CANCEL");
        assertEquals(fromAccount.getBalanceAmount(), BigDecimal.valueOf(11));
        assertEquals(fromAccount.getTransferAmount(), BigDecimal.ONE);
    }
}