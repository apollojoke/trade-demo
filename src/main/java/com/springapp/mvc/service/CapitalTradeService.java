package com.springapp.mvc.service;

import com.springapp.mvc.dto.TransferOrderDto;

/**
 * Created by ErikLee on 2019/5/6.
 */
public interface CapitalTradeService {
    String record(TransferOrderDto orderDto);
    void confirmRecord(TransferOrderDto orderDto);
    void cancelRecord(TransferOrderDto orderDto);
}
