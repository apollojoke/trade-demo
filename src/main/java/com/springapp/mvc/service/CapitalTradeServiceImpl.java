package com.springapp.mvc.service;

import com.springapp.mvc.dto.TransferOrderDto;
import com.springapp.mvc.entity.CapitalAccount;
import com.springapp.mvc.entity.TradeOrder;
import com.springapp.mvc.repository.CapitalAccountRepository;
import com.springapp.mvc.repository.TradeOrderRepository;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

/**
 * Created by ErikLee on 2019/5/6.
 */
@Service
public class CapitalTradeServiceImpl implements CapitalTradeService {

    @Autowired
    CapitalAccountRepository capitalAccountRepository;

    @Autowired
    TradeOrderRepository tradeOrderRepository;

    @Override
    @Transactional
    public String record(TransferOrderDto orderDto) {

        //should use debug level log here in prod
        System.out.println("transfer try called, time: " + DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));

        TradeOrder existTradeOrder = tradeOrderRepository.findByMerchantOrderNo(orderDto.getMerchantOrderNo());

        //if order has been record, return success directly
        if (existTradeOrder == null) {
            TradeOrder tradeOrder = new TradeOrder(orderDto.getFromUserId(), orderDto.getToUserId(), orderDto.getMerchantOrderNo(), orderDto.getAmount());
            //create a transfer order and freeze transfer capital
            //DataIntegrityViolationException may happen when insert trade order concurrently
            tradeOrderRepository.insert(tradeOrder);
            CapitalAccount fromAccount = capitalAccountRepository.findByUserId(tradeOrder.getFromUserId());
            fromAccount.transferFrom(tradeOrder.getAmount());
            capitalAccountRepository.save(fromAccount);
        }
        return "success";
    }

    @Override
    @Transactional
    public void confirmRecord(TransferOrderDto orderDto) {

        System.out.println("transfer confirm called, time: " + DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));

        TradeOrder tradeOrder = tradeOrderRepository.findByMerchantOrderNo(orderDto.getMerchantOrderNo());

        //make sure order exist and status is DRAFT, ensure idempotency.
        if (null != tradeOrder && "DRAFT".equals(tradeOrder.getStatus())) {
            // confirm order and transfer capital
            tradeOrder.confirm();
            tradeOrderRepository.update(tradeOrder);
            CapitalAccount transferToAccount = capitalAccountRepository.findByUserId(orderDto.getToUserId());
            transferToAccount.transferTo(orderDto.getAmount());
            capitalAccountRepository.save(transferToAccount);
        }
    }

    @Override
    @Transactional
    public void cancelRecord(TransferOrderDto orderDto) {

        System.out.println("transfer cancel called, time: " + DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));

        TradeOrder tradeOrder = tradeOrderRepository.findByMerchantOrderNo(orderDto.getMerchantOrderNo());

        //make sure order exist and status is DRAFT, ensure idempotency.
        if (null != tradeOrder && "DRAFT".equals(tradeOrder.getStatus())) {

            // cancel order and release capital
            tradeOrder.cancel();
            tradeOrderRepository.update(tradeOrder);

            CapitalAccount capitalAccount = capitalAccountRepository.findByUserId(orderDto.getFromUserId());
            capitalAccount.cancelTransfer(orderDto.getAmount());
            capitalAccountRepository.save(capitalAccount);
        }

    }
}
