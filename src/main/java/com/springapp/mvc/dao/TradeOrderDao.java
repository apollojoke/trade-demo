package com.springapp.mvc.dao;

import com.springapp.mvc.entity.TradeOrder;

/**
 * Created by ErikLee on 2019/5/6.
 */
public interface TradeOrderDao {

    int insert(TradeOrder tradeOrder);

    int update(TradeOrder tradeOrder);

    TradeOrder findByMerchantOrderNo(String merchantOrderNo);
}
