package com.springapp.mvc.dao;

import com.springapp.mvc.entity.CapitalAccount;

/**
 * Created by ErikLee on 2019/5/6.
 */
public interface CapitalAccountDao {

    CapitalAccount findByUserId(long userId);

    int update(CapitalAccount capitalAccount);
}
