package com.springapp.mvc;

import com.springapp.mvc.dto.TransferOrderDto;
import com.springapp.mvc.service.CapitalTradeService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.UUID;

@Controller
public class TransferController {

    @Autowired
    private CapitalTradeService capitalTradeService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        model.addAttribute("message", "Hello world!");
        return "hello";
    }

    @RequestMapping(value = "/api/v1/transfer", method = RequestMethod.GET)
    public String transfer(ModelMap model) {
        //为方便演示,参数校验代码未实现
        //接口规范:入参需包含:
        //timestamp -- 发送请求的时间，格式"yyyy-MM-dd HH:mm:ss"
        //app_id -- 应用ID,也可以理解为渠道号,用于校验接口调用方是否合法
        //sign_type -- 签名方式,常用的有RSA2和RSA,根据此字段做签名的验证
        //sign -- 签名字段, 使用用户私钥和sign_type中标注的签名方式,签名业务参数,防止参数被篡改,服务端需要使用用户公钥验签
        //biz_content -- 业务参数,JSON格式,所有请求参数在这个字段中传递
        //入参示例:
        //timestamp=2019-05-07 22:00:00&app_id=testId&sgin_type=RSA2&sign=XXXXXXX&biz_content={"fromUserId":"1001","toUserId":"1002","amount":"10.00","merchantOrderNo":"testNo","orderTitle":"testTitle"}

        //TODO 为了保证接口调用安全,要做如下校验
        //checkTimestamp();
        //checkAppId();
        //checkSign();

        TransferOrderDto testDto = new TransferOrderDto();
        testDto.setFromUserId(1001);
        testDto.setToUserId(1002);
        testDto.setAmount(BigDecimal.valueOf(15));
        testDto.setMerchantOrderNo(UUID.randomUUID().toString()); //调用方传入,需保证唯一
        testDto.setOrderTitle("testTitle");

        String result;
        try {
            //try阶段,调用record(),扣款,冻结转账金额,预留资源
            String tryResult = capitalTradeService.record(testDto);
            if ("success".equalsIgnoreCase(tryResult)) {
                //confirm阶段,冻结成功,则确认转账
                capitalTradeService.confirmRecord(testDto);
            }
            result = "transfer success";
        } catch (Exception e) {
            //cancel阶段,取消业务,释放预留资源
            capitalTradeService.cancelRecord(testDto);
            result = "transfer failed";
            //TODO 友好的返回各种异常信息
        }

        model.addAttribute("message", result);
        return "hello";
    }
}