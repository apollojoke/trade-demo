package com.springapp.mvc.entity;

import java.math.BigDecimal;

/**
 * Created by ErikLee on 2019/5/6.
 */
public class TradeOrder {

    private long id;

    //转出用户id
    private long fromUserId;

    //转入用户id
    private long toUserId;

    //商户订单号
    private String merchantOrderNo;

    private BigDecimal amount;

    private String status = "DRAFT";

    private long version = 1l;

    public TradeOrder() {
    }

    public TradeOrder(long fromUserId, long toUserId, String merchantOrderNo, BigDecimal amount) {
        this.fromUserId = fromUserId;
        this.toUserId = toUserId;
        this.merchantOrderNo = merchantOrderNo;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public long getFromUserId() {
        return fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getStatus() {
        return status;
    }

    public void confirm() {
        this.status = "CONFIRM";
    }

    public void cancel() {
        this.status = "CANCEL";
    }

    public long getVersion() {
        return version;
    }

    public void updateVersion() {
        this.version = version + 1;
    }

}
