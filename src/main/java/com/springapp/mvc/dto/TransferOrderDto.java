package com.springapp.mvc.dto;

import java.math.BigDecimal;

/**
 * Created by ErikLee on 2019/5/6.
 */
public class TransferOrderDto {

    //转出用户id
    private long fromUserId;

    //转入用户id
    private long toUserId;

    private String orderTitle;

    //商户订单号
    private String merchantOrderNo;

    private BigDecimal amount;

    public long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
